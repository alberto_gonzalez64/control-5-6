//Alberto Esteban Gonzalez Alfandi//
//19582326-k//
//paralelo 1//

#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>
#include <string.h>


float desvStd(estudiante curso[])
{
	int i ;
	float prom_g= 0, desviacion= 0, base, dif, dif_2, sum;
	for (i = 0; i < 3 ; i++)
	{
		if(curso[i].prom != 0.0)
		{
		prom_g= curso[i].prom;
		}
	}
	prom_g=prom_g/3;
	dif=curso[i].prom - prom_g;
	dif_2= dif *dif;
	sum = dif_2;
	base=sum/3;
	desviacion=sqrt(base);
	return desviacion;
}

float menor(estudiante curso[])
{
	int i = 0 ;
	float menor= 7.0;
	for (i = 0; i < 3 ; i++)
	{
		if (curso[i].prom <= menor)
		{
		menor=curso[i].prom ;
		}	
	}
	return menor;
}

float mayor(estudiante curso[])
{
	int i = 0;
	float mayor=1.0;
	for (i = 0; i < 3 ; i++)
	{
		if (curso[i].prom >= mayor)
		{
		mayor= curso[i].prom ;
		}
	}

	return mayor;
}

void registroCurso(estudiante curso[])
{
	int i = 0;
	float promedio;
	for (i = 0; i < 3 ; i++)
	{
		printf("\n %s %s %s\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
		printf("Ingrese las notas de los proyectos(3): \n--> ");
		scanf("%f %f %f", &curso[i].asig_1.proy1, &curso[i].asig_1.proy2, &curso[i].asig_1.proy3);
		printf("Ahora ingrese las notas de los controles(6): \n--> ");
		scanf("%f %f %f %f %f %f", &curso[i].asig_1.cont1, &curso[i].asig_1.cont2, &curso[i].asig_1.cont3, &curso[i].asig_1.cont4, &curso[i].asig_1.cont5, &curso[i].asig_1.cont6);
		promedio = (curso[i].asig_1.proy1 + curso[i].asig_1.proy2 + curso[i].asig_1.proy3+curso[i].asig_1.cont1 + curso[i].asig_1.cont2 + curso[i].asig_1.cont3 + curso[i].asig_1.cont4 + curso[i].asig_1.cont5 + curso[i].asig_1.cont6)/9;
		curso[i].prom=promedio;
	}
	
}

void clasificarEstudiantes(char path[], estudiante curso[])
{
	int i ;
	FILE *APROBADO;
	FILE *REPROBADO;
	APROBADO =fopen("estudiantes_aprobados.txt", "w");
	REPROBADO =fopen("estudiantes_reprobados.txt", "w");
	for (i = 0; i < 3 ; i++)
	{
		if (curso[i].prom < 4.0)
		{
			printf("reprobado %s %s %s\n",curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
			fprintf(REPROBADO, "\n\nreprobado %s %s %s\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
		}
		else
		{
			printf("aprobado %s %s %s\n",curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
			fprintf(APROBADO, "\n\naprobado %s %s %s\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
		}
	}
	fclose(APROBADO);
	fclose(REPROBADO);
}





void metricasEstudiantes(estudiante curso[])
{
	float desviacion = desvStd(curso), prom_mayor = mayor(curso) , prom_menor = menor(curso);
	printf("\n la desviacion estandar es: %3f\n el mejor promedio fue:%2f\nel menor promedio fue: %2f\n",desviacion, prom_mayor, prom_menor);

}






void menu(estudiante curso[])
{
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main()
{
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}

